define(['backbone', 'models/Photo'], function (Backbone, Photo) {
    return Backbone.Collection.extend({
        model: Photo,
        url: 'http://demo7491943.mockable.io/api/get_all/',
        sortAttribute: "id",
        sortDirection: 1,
        sortPhoto: function (attr) {
            this.sortAttribute = attr;
            this.sort();
        },
        comparator: function(a, b) {
        var a = a.get(this.sortAttribute),
            b = b.get(this.sortAttribute);
        if (a == b) return 0;
        if (this.sortDirection == 1) {
            return a > b ? 1 : -1;
        } else {
            return a < b ? 1 : -1;
        }
    }
 })

});