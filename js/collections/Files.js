define(['backbone', 'models/File'], function (Backbone, File) {
    return Backbone.Collection.extend({
        model: File,
     url: 'http://demo7491943.mockable.io/api/get_list'});
});