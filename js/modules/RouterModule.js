define(['backbone','marionette'], function (Backbone,Marionette) {
    var Router = Marionette.AppRouter.extend({
        appRoutes: {
            'files' : 'goFiles',
            'photos': 'goPhotos',
            'sharing' : 'goSharing',
            'links' : 'goLinks',
            'events' : 'goEvents',
            'getstarted' : 'goGetStarted'
        }

    });

    return Router;
});