define(['backbone', 'marionette', 'views/Layout', 'views/Files', 'views/Photostemplate', 'views/Sharing',

    'views/Links', 'views/GetStarted', 'views/PhotosList','views/Events','views/FilesList'], function (Backbone, Marionette, Layout, Files, Photostemplate, Sharing, Links, GetStarted,
                                                                                    PhotoList,Events,FilesList) {


    var Controller = Marionette.Controller.extend({
        initialize: function (options) {

            this.content = options.content;
        },
        start: function () {

        },
        goFiles: function () {


            this.content.show(new Files);
            $(".temp_files").html( FilesList.render().$el);

        },
        goPhotos: function () {

            this.content.show(new Photostemplate);
            $(".temp").html( PhotoList.render().$el);
            PhotoList.drag();
        },

        goSharing: function () {

            this.content.show(new Sharing);
        },
        goLinks: function () {

            this.content.show(new Links);
        },
        goSharing: function () {

            this.content.show(new Sharing);
        },
        goLinks: function () {

            this.content.show(new Links);
        },

        goGetStarted: function () {

            this.content.show(new GetStarted);
        },
        goEvents: function () {

            this.content.show(new Events);
        }

    });

    return Controller;
});