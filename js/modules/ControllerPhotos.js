define(['backbone', 'marionette', 'views/Layout', 'views/Photostemplate', 'views/PhotosList'], function (Backbone, Marionette, Layout, Photostemplate, PhotoList) {


    var ControllerPhotos = Marionette.Controller.extend({
        initialize: function (options) {

            this.content = options.content;
        },
        start: function () {

        },
        goPhotos: function () {
            this.content.show(new Photostemplate);
            PhotoList.drag();
        }

    });

    return ControllerPhotos;
});