
define(['backbone','marionette','views/Layout','views/Files','views/Photostemplate','views/Sharing',

    'views/Links','views/GetStarted','views/PhotosList', 'modules/RouterModule', 'modules/ControllerModule','views/FileRow','views/FilesList'], function (Backbone,Marionette,Layout,Files,
                                                                                                                        Photostemplate,Sharing,Links,GetStarted,PhotoList,RouterModule,ControllerModule,FileRow,FilesList) {
        var DropBoxApp = new Marionette.Application();


        DropBoxApp.addRegions({
            content: '.content-template',
            sidebar: '.sidebar'
        });


        var controller = new ControllerModule({
            content: DropBoxApp.content
        });


        DropBoxApp.router = new RouterModule({
            controller : controller
        });


        DropBoxApp.addInitializer(function(options) {});
        DropBoxApp.on("before:start", function(){
            var layout = new Layout;
            $('.insert').append(layout.render().el);
        });
        DropBoxApp.on("start", function(){
        DropBoxApp.content.show(new Photostemplate);
        $('.buttonmenu').click(function () {
            ($('.sidebar').css('display') == 'none')?$('.sidebar').show():$('.sidebar').hide();
        });

        Backbone.history.start();

    });

    return DropBoxApp;

});