define(['backbone','marionette','views/Layout','views/Files','views/Photostemplate','views/Sharing',

    'views/Links','views/GetStarted','views/PhotosList'], function (Backbone,Marionette,Layout,Files,
                                                                    Photostemplate,Sharing,Links,GetStarted,PhotoList) {



        var layout  = new Layout;
        return Marionette.AppRouter.extend({

            routes : {
                'files' : 'goFiles',
                'photos': 'goPhotos',
                'sharing' : 'goSharing',
                'links' : 'goLinks',
                'events' : 'goEvents',
                'getstarted' : 'goGetStarted'
            },
            goFiles: function(){

                layout.content.show(new Files);
            },
            goPhotos: function(){
              //  $(".temp").html("");
                layout.content.show(new Photostemplate);

                $(".temp").html(PhotoList.render().$el);

                $('.temp').appendTo('.photocontent');
            },

            goSharing: function(){

                layout.content.show(new Sharing);
            },
            goLinks: function(){

                layout.content.show(new Links);
            },
            goSharing: function(){

                layout.content.show(new Sharing);
            },
            goLinks: function(){

                layout.content.show(new Links);
            },

            goGetStarted: function(){

                layout.content.show(new GetStarted);
            }


        });


});

