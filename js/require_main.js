requirejs.config({
    paths:{
        'jquery': 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min',
        'jqueryui': 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min',
        'underscore':  '../bower_components/underscore/underscore',
        'backbone' : '../bower_components/backbone/backbone',
        'marionette' : '../bower_components/marionette/lib/backbone.marionette',
        'lightbox' : 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.7.1/js/lightbox'

    },
    shim: {
        'jquery': {
            exports: 'jQuery'
        },
        'jqueryui': {
            deps: ['jquery']
        },
        'underscore': {
            exports: '_'
        },
        'lightbox':{
            deps: ['jquery']
        },
        'backbone': {
            deps: ['underscore', 'jquery','jqueryui', ,'lightbox'],
            exports: 'Backbone'
        },
        'marionette' : {
            exports : 'Backbone.Marionette',
            deps : ['backbone']
        }

    }
});

require(["jquery", "backbone", "marionette",'app'],
    function($, Backbone, Marionette,App) {
        App.start({});

    });