define(['backbone','collections/Photos'], function (Backbone, Photos) {
    return Backbone.View.extend({
                initialize: function () {
                    var _this = this;
                    var cPhotos = new Photos();
                    this.model = cPhotos;
                    cPhotos.fetch({
                        success: function() {
                            _this.render();
                        }
                    });

                },
                el: ".temp",
                template: _.template($('#templatephoto').html()),
                render: function() {

                    _.each(this.model.models, function(photos){
                        var photosTemplate = this.template(photos.toJSON());
                        $(this.el).append(photosTemplate);
                    }, this);
                    var c = {};
                    $( ".item" ).draggable({
                        containment:'document',
                        cursor: 'crosshair',
                        revert: true,
                        zIndex: 1    ,
                        start: function (event, ui) {
                            c.tr = this;
                            c.helper = ui.helper;
                            c.parent = $(this).parent();

                        }
                    });
                    $('.dropdelete').droppable( {
                        drop:  function handleDropEvent( event, ui ) {
                            var draggable = ui.draggable;

                            if( confirm( "Are you sure want to remove this file? This action cannot be undone." )){
                                $(c.tr).detach();
                            }
                        }
                    } );
                    return this;

                }
    });
});


