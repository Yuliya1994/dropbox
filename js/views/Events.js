define(['backbone','marionette'], function (Backbone,Marionette) {
    return Marionette.ItemView.extend({
        template: '#events-template'
    });});