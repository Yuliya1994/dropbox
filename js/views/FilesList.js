define(['backbone','marionette','views/FileRow','collections/Files'], function (Backbone,Marionette,FileRow,Files) {
    var FileList = Marionette.CompositeView.extend({
        template: '#files-list',
        childView:FileRow,
        tagName:'ol',
         onRender: function() {
            $('.temp_files').appendTo('.filescontent');
        }
    });
    var files =  new Files();
    var fileList = new FileList({ collection: files });
    files.fetch({
        success: function() {
          //  $(".temp_files").html(fileList.render().$el);


        }
    });
    return fileList;
});