define(['backbone','marionette'], function (Backbone,Marionette) {
    return  Marionette.ItemView.extend({
        template: '#files-row'
    });
});