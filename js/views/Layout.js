define(['backbone','marionette'], function (Backbone,Marionette) {
    return Marionette.LayoutView.extend({
    template: '#layout-template',
    regions: {
        content: '.content-template',
        sidebar: '.sidebar'
    }

});

});