define(['backbone','marionette','views/PhotoRow','collections/Photos'], function (Backbone,Marionette,PhotoRow,Photos) {
    var PhotoList = Marionette.CompositeView.extend({
        template: '#photos-list',
        tagName:    'main',
        childView:PhotoRow,
        initialize: function() {
            this.listenTo(this.collection, "sort", this.update);
            this.listenTo(this.collection, "remove");
        },
        events: {
            "click .sort button": "sort_button"
        },
        sort_button: function(e){
            var $el = $(e.currentTarget);
            var sort_type = $el.data('sort-by');
            this.collection.sortPhoto(sort_type);
            this.drag();
        },
        update: function () {
        },
        drag: function(){
            var _this = this;
            var c = {};
            $( ".item" ).draggable({
                containment:'document',
                cursor: 'crosshair',
                revert: true,
                zIndex: 1    ,
                start: function (event, ui) {
                    c.tr = this;
                    c.helper = ui.helper;
                    c.parent = $(this).parent();

                }
            });
            $('.dropdelete').droppable( {
                drop:  function handleDropEvent( event, ui ) {
                    var draggable = ui.draggable;
                    if( confirm( "Are you sure want to remove this file? This action cannot be undone." )){
                        _this.collection.remove($(c.tr).data('id'));

                    }
                }
            });
        },
        onRender: function() {
            $('.temp').appendTo('.photocontent');
        }
    });
    var photos =  new Photos();
    var photoList = new PhotoList({ collection: photos });
    photos.fetch({
        success: function() {
            $(".temp").html(photoList.render().$el);
            photoList.drag();
            $(document).on("mouseover",".item",function(){
                $(this).find('.show').show();
            });

            $(document).on("mouseout",".item",function(){
                $(this).find('.show').hide();
            });
        }

    });

    return photoList;
});