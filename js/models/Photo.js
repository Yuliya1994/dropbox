define(['backbone'], function (Backbone) {
    return Backbone.Model.extend({
        defaults: {
            id: 0,
            name: "none",
            size: "0MB",
            modified: "none",
            preview: ""
        }
    });
});